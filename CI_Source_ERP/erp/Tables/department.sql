﻿CREATE TABLE [erp].[department] (
    [department_id]   INT           IDENTITY (1, 1) NOT NULL,
    [department_name] VARCHAR (128) NOT NULL,
    CONSTRAINT [PK_department] PRIMARY KEY CLUSTERED ([department_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_erp_department_department_name]
    ON [erp].[department]([department_name] ASC);

