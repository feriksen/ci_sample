﻿CREATE TABLE [erp].[sales_invoice] (
    [sales_invoice_id] INT          IDENTITY (1, 1) NOT NULL,
    [sales_invoice_no] VARCHAR (10) NOT NULL,
    [sales_order_id]   INT          NOT NULL,
    [date_invoice]     DATE         NULL,
    [date_due]         DATE         NULL,
    [date_created]     DATETIME     NULL,
    [date_modified]    DATETIME     NULL,
    CONSTRAINT [PK_sales_invoice] PRIMARY KEY CLUSTERED ([sales_invoice_id] ASC),
    CONSTRAINT [FK_sales_invoice_sales_order_header] FOREIGN KEY ([sales_order_id]) REFERENCES [erp].[sales_order_header] ([sales_order_id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_erp_sales_invoice_sales_invoice_no]
    ON [erp].[sales_invoice]([sales_invoice_no] ASC);

