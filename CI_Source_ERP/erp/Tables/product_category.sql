﻿CREATE TABLE [erp].[product_category] (
    [product_category_id] INT           IDENTITY (1, 1) NOT NULL,
    [category_name]       VARCHAR (128) NOT NULL,
    CONSTRAINT [PK_product_category] PRIMARY KEY CLUSTERED ([product_category_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_erp_product_category_category_name]
    ON [erp].[product_category]([category_name] ASC);

