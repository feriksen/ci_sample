﻿CREATE TABLE [erp].[sales_order_header] (
    [sales_order_id]  INT          IDENTITY (1, 1) NOT NULL,
    [sales_order_no]  VARCHAR (10) NOT NULL,
    [customer_id]     INT          NOT NULL,
    [sales_person_id] INT          NOT NULL,
    [date_created]    DATETIME     NULL,
    [date_modified]   DATETIME     NULL,
    CONSTRAINT [PK_sales_order_header] PRIMARY KEY CLUSTERED ([sales_order_id] ASC),
    CONSTRAINT [FK_sales_order_header_customer] FOREIGN KEY ([customer_id]) REFERENCES [erp].[customer] ([customer_id]),
    CONSTRAINT [FK_sales_order_header_employee] FOREIGN KEY ([sales_person_id]) REFERENCES [erp].[employee] ([employee_id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_erp_sales_order_header_sales_order_no]
    ON [erp].[sales_order_header]([sales_order_no] ASC);

