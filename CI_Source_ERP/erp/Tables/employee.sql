﻿CREATE TABLE [erp].[employee] (
    [employee_id]    INT           IDENTITY (1, 1) NOT NULL,
    [employee_login] VARCHAR (128) NOT NULL,
    [name_first]     VARCHAR (128) NULL,
    [name_last]      VARCHAR (128) NULL,
    [department_id]  INT           NOT NULL,
    CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED ([employee_id] ASC),
    CONSTRAINT [FK_employee_department] FOREIGN KEY ([department_id]) REFERENCES [erp].[department] ([department_id])
);

