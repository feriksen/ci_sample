﻿CREATE TABLE [erp].[product] (
    [product_id]          INT           IDENTITY (1, 1) NOT NULL,
    [product_no]          VARCHAR (10)  NOT NULL,
    [product_category_id] INT           NOT NULL,
    [product_name]        VARCHAR (128) NOT NULL,
    [unit_price]          MONEY         NULL,
    CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED ([product_id] ASC),
    CONSTRAINT [FK_product_product_category] FOREIGN KEY ([product_category_id]) REFERENCES [erp].[product_category] ([product_category_id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_erp_product_product_no]
    ON [erp].[product]([product_no] ASC);

