﻿CREATE TABLE [erp].[sales_order_line] (
    [sales_order_line_id] INT   IDENTITY (1, 1) NOT NULL,
    [sales_order_id]      INT   NOT NULL,
    [line_no]             INT   NOT NULL,
    [product_id]          INT   NOT NULL,
    [qty]                 INT   NOT NULL,
    [unit_price]          MONEY NULL,
    CONSTRAINT [PK_sales_order_line] PRIMARY KEY CLUSTERED ([sales_order_line_id] ASC),
    CONSTRAINT [FK_sales_order_line_product] FOREIGN KEY ([product_id]) REFERENCES [erp].[product] ([product_id]),
    CONSTRAINT [FK_sales_order_line_sales_order_header] FOREIGN KEY ([sales_order_id]) REFERENCES [erp].[sales_order_header] ([sales_order_id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_erp_sales_order_line_line_no]
    ON [erp].[sales_order_line]([line_no] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_erp_sales_order_line_order_product]
    ON [erp].[sales_order_line]([sales_order_id] ASC, [product_id] ASC);

