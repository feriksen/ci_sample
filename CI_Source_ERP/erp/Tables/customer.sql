﻿CREATE TABLE [erp].[customer] (
    [customer_id]  INT           IDENTITY (1, 1) NOT NULL,
    [customer_no]  VARCHAR (10)  NOT NULL,
    [company_name] VARCHAR (128) NOT NULL,
    [street]       VARCHAR (128) NOT NULL,
    [postal_code]  VARCHAR (10)  NOT NULL,
    [city]         VARCHAR (128) NOT NULL,
    [country]      VARCHAR (128) NOT NULL,
    CONSTRAINT [PK_customer] PRIMARY KEY CLUSTERED ([customer_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_erp_customer_customer_no]
    ON [erp].[customer]([customer_no] ASC);

