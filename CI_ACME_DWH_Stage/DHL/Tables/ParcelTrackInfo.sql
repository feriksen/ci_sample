﻿CREATE TABLE [DHL].[ParcelTrackInfo] (
    [parcelNumber]   VARCHAR (10) NOT NULL,
    [customerRefNo]  VARCHAR (10) NOT NULL,
    [eventName]      VARCHAR (20) NOT NULL,
    [eventTimestamp] DATETIME     NOT NULL,
    [LOAD_TK]        INT          NOT NULL
);

