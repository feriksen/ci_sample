﻿CREATE TABLE [UPS].[ParcelTrackInfo] (
    [parcelId]         BIGINT       NOT NULL,
    [customerRefNo]    VARCHAR (10) NOT NULL,
    [eventDescription] VARCHAR (20) NOT NULL,
    [eventTimestamp]   DATETIME     NOT NULL,
    [LOAD_TK]          INT          NOT NULL
);

