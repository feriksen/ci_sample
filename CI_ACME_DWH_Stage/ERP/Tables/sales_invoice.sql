﻿CREATE TABLE [ERP].[sales_invoice] (
    [sales_invoice_id] INT          NOT NULL,
    [sales_invoice_no] VARCHAR (10) NOT NULL,
    [sales_order_id]   INT          NOT NULL,
    [date_invoice]     DATE         NULL,
    [date_due]         DATE         NULL,
    [date_created]     DATETIME     NULL,
    [date_modified]    DATETIME     NULL,
    [LOAD_TK]          INT          NOT NULL
);

