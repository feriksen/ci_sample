﻿CREATE TABLE [ERP].[employee] (
    [employee_id]    INT           NOT NULL,
    [employee_login] VARCHAR (128) NOT NULL,
    [name_first]     VARCHAR (128) NULL,
    [name_last]      VARCHAR (128) NULL,
    [department_id]  INT           NOT NULL,
    [LOAD_TK]        INT           NOT NULL
);

