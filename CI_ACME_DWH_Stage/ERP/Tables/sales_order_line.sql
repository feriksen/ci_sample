﻿CREATE TABLE [ERP].[sales_order_line] (
    [sales_order_line_id] INT   NOT NULL,
    [sales_order_id]      INT   NOT NULL,
    [line_no]             INT   NOT NULL,
    [product_id]          INT   NOT NULL,
    [qty]                 INT   NOT NULL,
    [unit_price]          MONEY NULL,
    [LOAD_TK]             INT   NOT NULL
);

