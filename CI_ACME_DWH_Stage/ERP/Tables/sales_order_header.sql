﻿CREATE TABLE [ERP].[sales_order_header] (
    [sales_order_id]  INT          NOT NULL,
    [sales_order_no]  VARCHAR (10) NOT NULL,
    [customer_id]     INT          NOT NULL,
    [sales_person_id] INT          NOT NULL,
    [date_created]    DATETIME     NULL,
    [date_modified]   DATETIME     NULL,
    [LOAD_TK]         INT          NOT NULL
);

