﻿CREATE TABLE [ERP].[customer] (
    [customer_id]  INT           NOT NULL,
    [customer_no]  VARCHAR (10)  NOT NULL,
    [company_name] VARCHAR (128) NOT NULL,
    [street]       VARCHAR (128) NOT NULL,
    [postal_code]  VARCHAR (10)  NOT NULL,
    [city]         VARCHAR (128) NOT NULL,
    [country]      VARCHAR (128) NOT NULL,
    [LOAD_TK]      INT           NOT NULL
);

