﻿CREATE TABLE [ERP].[product] (
    [product_id]          INT           NOT NULL,
    [product_no]          VARCHAR (10)  NOT NULL,
    [product_category_id] INT           NOT NULL,
    [product_name]        VARCHAR (128) NOT NULL,
    [unit_price]          MONEY         NULL,
    [LOAD_TK]             INT           NOT NULL
);

