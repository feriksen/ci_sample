﻿CREATE TABLE [DWH].[h_customer] (
    [customer_tk] INT          IDENTITY (1, 1) NOT NULL,
    [customer_no] VARCHAR (10) NOT NULL,
    [last_seen]   INT          NULL,
    [load_tk]     INT          NULL
);

