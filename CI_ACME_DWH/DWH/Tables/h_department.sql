﻿CREATE TABLE [DWH].[h_department] (
    [department_tk]   INT           IDENTITY (1, 1) NOT NULL,
    [department_name] VARCHAR (128) NOT NULL,
    [last_seen]       INT           NULL,
    [load_tk]         INT           NULL
);

