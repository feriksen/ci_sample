﻿CREATE TABLE [DWH].[h_product] (
    [product_tk] INT          IDENTITY (1, 1) NOT NULL,
    [product_no] VARCHAR (10) NOT NULL,
    [last_seen]  INT          NULL,
    [load_tk]    INT          NULL
);

