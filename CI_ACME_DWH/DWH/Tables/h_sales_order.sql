﻿CREATE TABLE [DWH].[h_sales_order] (
    [sales_order_tk] INT          IDENTITY (1, 1) NOT NULL,
    [sales_order_no] VARCHAR (10) NOT NULL,
    [last_seen]      INT          NULL,
    [load_tk]        INT          NULL
);

