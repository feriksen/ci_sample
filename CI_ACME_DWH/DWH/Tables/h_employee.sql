﻿CREATE TABLE [DWH].[h_employee] (
    [employee_tk]    INT           IDENTITY (1, 1) NOT NULL,
    [employee_login] VARCHAR (128) NOT NULL,
    [last_seen]      INT           NULL,
    [load_tk]        INT           NULL
);

