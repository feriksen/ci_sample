# Background

This is a sample solution that will show how you can use Continous Integration within the different stages of an end-to-end BI solution.

Some highlights:
- Database version control
- Automated testing (with NBi)
- Controlled deployment options



## End to End

By end to end, this is what is included:

- ETL from source systems into a common Staging DB
- Tests of ETL packages into Staging DB
- Tests of Staging DB structure
- ETL from staging DB into an Data Vault Data Warehouse
- Tests of ETL packages into Data Warehouse DB
- Tests of Staging DB structure
- Tests of Data Warehouse DB structure
- ETL from Data Vault DWH into a star-schema based Data Mart
- Tests of ETL packages into Data Mart DB
- Tests of Data Warehouse DB structure
- Tests of Data Mart DB structure
- Olap cube(s) on top of Data Mart
- Tests of Olap cube structure
- Reporting Services reports on top of Olap cube(s)
- Tests of SSRS Shared Datasets
- Tests of SSRS Reports

Note that the purpose of the included samples are *not* to show a "complete" how-to in terms of best-practise etc. They merely serve to show the value and potential usage of Continous Integration and tool support for BI solutions.